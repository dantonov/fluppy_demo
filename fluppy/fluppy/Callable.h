//
//  Callable.h
//  fluppy
//
//  Created by Dmitry Antonov on 25/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef Callable_h
#define Callable_h

/// Базовый класс для создания коллбеков через сигналы
class Callable
{
public:
	inline Callable() {}
	inline virtual ~Callable() throw() {}
};

#endif /* Callable_h */
