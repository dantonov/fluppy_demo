//
//  SimpleSignal.h
//  fluppy
//
//  Created by Dmitry Antonov on 25/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef SimpleSignal_h
#define SimpleSignal_h

#include <vector>
#include <functional>
#include "Callable.h"

/// Простой сигнал в котором можно передать несколько аргументов
template <typename ...ARGS>
class SimpleSignal
{
public:
	typedef void(Callable::*METHOD)(ARGS...);

	inline SimpleSignal() {}
	virtual ~SimpleSignal() throw() {}

	/// Вызвать сигнал на всех подписчиков
	void emit(ARGS... args);
	/// Оператор вызова
	inline void operator()(ARGS... args) { emit(args...); }

	/// Подписаться на сигнал
	template <typename CTYPE>
	inline void connect(Callable * obj, void (CTYPE::*method)(ARGS...))
	{
		METHOD baseMethod = reinterpret_cast<METHOD>(method);
		connect(obj, baseMethod);
	}
	/// Отписаться от сигнала
	template <typename CTYPE>
	inline void disconnect(Callable * obj, void (CTYPE::*method)(ARGS...))
	{
		METHOD baseMethod = reinterpret_cast<METHOD>(method);
		disconnect(obj, baseMethod);
	}

private:
	/// Список коллбеков
	std::vector<std::pair<Callable *,std::function<void(Callable &, ARGS...)> > > mCallbacks;

	/// Соединиться через базовый класс
	void connect(Callable * obj, METHOD method);
	/// Соединиться через базовый класс
	void disconnect(Callable * obj, METHOD method);
};

template <typename ...ARGS>
void SimpleSignal<ARGS...>::emit(ARGS... args)
{
	for (auto & pair : mCallbacks)
		pair.second(*pair.first, args...);
}

template <typename ...ARGS>
void SimpleSignal<ARGS...>::connect(Callable * obj, METHOD method)
{
	// В данной реализации не контролирует подписан ли уже данный метод
	mCallbacks.push_back(std::make_pair(obj, method));
}

template <typename ...ARGS>
void SimpleSignal<ARGS...>::disconnect(Callable * obj, METHOD method)
{
	// В данной реализации удаление из вызванного коллбека является небезопасныс
	for (auto it = mCallbacks.begin(); it != mCallbacks.end(); ++it)
	{
		if (it->first != obj)
			continue;
		if (*(it->second.template target<METHOD>()) != method)
			continue;

		mCallbacks.erase(it);
		break;
	}
}


#endif /* SimpleSignal_h */
