//
//  GameObject.cpp
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#include "GameObject.h"
#include "GameManager.h"
#include "ShaderCache.h"
#include "GameManager.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

GameObject::GameObject()
{
	setAdditiveColor(aux::colorWhite);
}

GameObject::~GameObject() throw()
{
}

void GameObject::setPosition(const Position & pos)
{
	Rect rect = GameObject::rect();
	rect.pos = pos;
	setRect(rect);
}

void GameObject::setRect(const Rect & rect)
{
	if (mRect.size != rect.size)
		setRefreshBuffersDataTrue();

	mRect = rect;
	mModelMx = glm::mat4x4(1.0);
	mModelMx = glm::translate(mModelMx, {mRect.pos.x, mRect.pos.y, mRect.pos.z});
}

Rect GameObject::rect() const
{
	return mRect;
}

void GameObject::setRefreshBuffersDataTrue()
{
	mRefreshBuffersData = true;
}

void GameObject::setAdditiveColor(const Color & color)
{
	mAdditiveColor = color;
}

void GameObject::update()
{
}

void GameObject::draw()
{
	doDraw();

	const Position near = mRect.pos;
	const Position far = mRect.pos + mRect.size;
	for (const std::unique_ptr<GameObject> & child : mObjects)
	{
		Rect chRect = child->rect();
		const Position & chNear = chRect.pos;
		const Position chFar = chRect.pos + chRect.size;
		// Отсечение того что не попадает в размеры объекта
		if (chFar.x < near.x || chNear.x > far.x || chFar.y < near.y || chNear.y > far.y)
			continue;

		child->draw();
	}
}

void GameObject::check()
{
}

void GameObject::addChild(GameObject * obj)
{
	obj->mParent = this;
	size_t size = mObjects.size();
	mObjects.resize(size + 1);
	mObjects.back().reset(obj);
}

void GameObject::removeFromParent()
{
	std::vector<std::unique_ptr<GameObject> > & objs = mParent->mObjects;
	for (auto it = objs.begin(); it != objs.end(); ++it)
	{
		if (it->get() != this)
			continue;

		objs.erase(it);
		break;
	}
}

void GameObject::refreshBuffers()
{
	std::vector<Position> vertices = {Position(0.0f, 0.0f), Position(0.0f, mRect.size.height),
		Position(mRect.size.width, 0.0f), mRect.size};
	std::vector<unsigned short> indices = {0, 1, 2, 1, 2, 3};
	mIndexSize = indices.size();

	if (mVertexBufferId == 0 && mIndexBufferId == 0)
	{
		glGenBuffers(1, &mVertexBufferId);
		glGenBuffers(1, &mIndexBufferId);

		glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Position) * vertices.size(), &vertices[0], GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBufferId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * mIndexSize, &indices[0], GL_DYNAMIC_DRAW);
	}
	else
	{
		assert(mVertexBufferId != 0 && mIndexBufferId != 0);
		// glBufferSubData
	}

	mRefreshBuffersData = false;
}

void GameObject::doDraw()
{
	if (mRefreshBuffersData)
		refreshBuffers();

	SManager()->shaderCache()->enableProgram(mShaderName);
	GLuint progId = SManager()->shaderCache()->lastActiveProgId();

	GLint modelUniformId = glGetUniformLocation(progId, "uModelMX");
	glUniformMatrix4fv(modelUniformId, 1, GL_FALSE, glm::value_ptr(mModelMx));

	GLint addColorUniformId = glGetUniformLocation(progId, "uAdditiveColor");
	glUniform4fv(addColorUniformId, 1, &mAdditiveColor.toGlColor()[0]);

	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBufferId);

    GLint vertLocation = glGetAttribLocation(progId, "aPosition");
	glVertexAttribPointer(vertLocation, 3, GL_FLOAT, GL_FALSE, sizeof(Position), 0);

	glEnableVertexAttribArray(vertLocation);
	//
	glDrawElements(GL_TRIANGLE_STRIP, (int)mIndexSize, GL_UNSIGNED_SHORT, 0);
	//
	glDisableVertexAttribArray(vertLocation);

	GLint error = glGetError();
	switch (error)
	{
	case GL_INVALID_VALUE:
		printf("GL_INVALID_VALUE\n");
		break;
	case GL_INVALID_OPERATION:
		printf("GL_INVALID_OPERATION\n");
		break;
	default:
		break;
	};
}
