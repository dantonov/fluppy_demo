//
//  TaskDelegate.h
//  fluppy
//
//  Created by Dmitry Antonov on 26/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef TaskDelegate_h
#define TaskDelegate_h

#include <Callable.h>
#include <functional>
#include <typeinfo>

/// Абстракция для делегата отложенного вызова
class ADelegate
{
public:
	inline ADelegate() {}
	inline virtual ~ADelegate() throw() {}

	virtual void call() = 0;
};

/// Структура для развертывания аргументов
template <int NUM, typename CALLBACK, typename TUPLE, typename ...ARGS>
struct ExpandWrapper
{
	static void expand(Callable * target, CALLBACK callback, TUPLE tuple, ARGS... args)
	{
		typedef typename std::tuple_element<NUM, TUPLE>::type ELTYPE;

		auto tElement = std::get<NUM>(tuple);
		ExpandWrapper<NUM - 1, CALLBACK, TUPLE, ELTYPE, ARGS...>::expand(target, callback, tuple, tElement, args...);
	}
};

/// Partial Specialization для вызова метода
template <typename CALLBACK, typename TUPLE, typename ...ARGS>
struct ExpandWrapper<-1, CALLBACK, TUPLE, ARGS...>
{
	static void expand(Callable * target, CALLBACK callback, TUPLE tuple, ARGS... args)
	{
		(target->*callback)(args...);
	}
};

/// Делегат отложенного вызова задачи
/// Хранит коллбек на метод класса и аргументы для передачи
template <typename ...ARGS>
class TaskDelegate : public ADelegate
{
public:
	inline TaskDelegate(Callable * target, void (Callable::*METHOD)(ARGS...), ARGS... args)
		: mTarget(target), mCall(METHOD), mArgs(args...) {}
	inline ~TaskDelegate() throw() {}

	/// Вызвать метод с аргументами
	void call() override;

private:
	/// Объект для вызова
	Callable * mTarget = nullptr;
	/// Метод для вызова
	void (Callable::* mCall)(ARGS...) = nullptr;
	/// Аргументы
	std::tuple<ARGS...> mArgs;

	/// Начало развертывания аргументов
	void expandTuple();
};

template <typename ...ARGS>
void TaskDelegate<ARGS...>::call()
{
	int const tupleSize = std::tuple_size<std::tuple<ARGS...>>::value;
	ExpandWrapper<tupleSize - 1, void (Callable::*)(ARGS...), std::tuple<ARGS...> >::expand(mTarget, mCall, mArgs);
}

#endif /* TaskDelegate_h */
