//
//  GLESView.h
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef GLESView_h
#define GLESView_h

#import <GLKit/GLKit.h>

@interface GLESView : GLKView
{
	GLuint mRenderBuffer;
}

@property (nonatomic, retain) NSTimer * renderTimer;

-(id) initWithFrame: (CGRect) rect;
-(EAGLContext *) createContext;

/// Отрисовка и апдейт происходят здесь
-(void) drawRect:(CGRect)rect;
/// Сюда приходят тапы
-(void) onTapCallabck: (UITapGestureRecognizer *) tapGR;

-(void) dealloc;

@end

#endif /* GLESView_h */
