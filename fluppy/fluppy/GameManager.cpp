//
//  StartManager.c
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#include "GameManager.h"
#include <assert.h>
#include "GameField.h"
#include "ShaderCache.h"
#include "TaskCaller.h"
#include <glm/gtc/matrix_transform.hpp>

static GameManager * sCurrentManager = nullptr;
GameManager * SManager()
{
	return sCurrentManager;
}

void GameManager::create()
{
	new GameManager();
}

GameManager * GameManager::instance()
{
	return sCurrentManager;
}

GameManager::GameManager()
{
	assert(sCurrentManager == nullptr);
	sCurrentManager = this;

	schedule(START);
}

GameManager::~GameManager() throw()
{
	commitStop();

	assert(this == sCurrentManager);
	sCurrentManager = nullptr;
}

void GameManager::onTap(float x, float y)
{
	if (!mGameField)
		return;

	Fluppy * fluppy = mGameField->fluppy();
	if (!fluppy)
		return;

	fluppy->jump();
}

void GameManager::schedule(ScheduleAction action)
{
	mCurrentAction = action;
}

void GameManager::update()
{
	ScheduleAction action = mCurrentAction;
	mCurrentAction = NONE;

	switch (action)
	{
	case NONE:
		break;
	case START:
		commitStart();
		break;
	case STOP:
		commitStop();
		break;
	case RESTART:
		commitRestart();
		break;
	}

	updateContent();
}

Size GameManager::screenSize() const
{
	return mScreenSize;
}

void GameManager::setScreenSize(const Size & size)
{
	assert(!size.isZero());

	mScreenSize = size;
	glViewport(0, 0, mScreenSize.width, mScreenSize.height);
	updateProjectionMx();
}

const glm::mat4x4 & GameManager::projectionMx() const
{
	return mProjectionMx;
}

const glm::mat4x4 & GameManager::viewMx() const
{
	return mViewMx;
}

GameField * GameManager::gameField()
{
	return mGameField;
}

WindowSystem * GameManager::windowSystem()
{
	return mWindowSystem;
}

ShaderCache * GameManager::shaderCache()
{
	return mShaderCache;
}

TaskCaller * GameManager::mainThreadCaller()
{
	return mMainCaller;
}

void GameManager::updateContent()
{
	if (mGameField)
	{
		mGameField->update();
		mGameField->draw();
		mGameField->check();
	}

	if (mMainCaller)
		mMainCaller->step();
}

void GameManager::commitStart()
{
	if (!mMainCaller)
		mMainCaller = new TaskCaller();
	if (!mShaderCache)
		mShaderCache = new ShaderCache();
	if (!mGameField)
	{
		mGameField = new GameField();
		mGameField->onRestartGame.connect(this, &GameManager::startNewGame);
	}
}

void GameManager::commitStop()
{
	if (mGameField)
		delete mGameField;
	mGameField = nullptr;
	if (mShaderCache)
		delete mShaderCache;
	mShaderCache = nullptr;
	if (mMainCaller)
		delete mMainCaller;
	mMainCaller = nullptr;
}

void GameManager::commitRestart()
{
	commitStop();
	commitStart();
}

void GameManager::updateProjectionMx()
{
	if (mScreenSize.isZero())
		return;

	mProjectionMx = glm::ortho(0.0f, mScreenSize.width, 0.0f, mScreenSize.height);
}

void GameManager::startNewGame(int value)
{
	schedule(RESTART);
}
