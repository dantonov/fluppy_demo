//
//  ShaderCache.cpp
//  fluppy
//
//  Created by Dmitry Antonov on 24/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#include "ShaderCache.h"
#include "AbstractDevice.h"
#include "GameManager.h"
#include "aux.h"
#include <cstdlib>
#include <glm/gtc/type_ptr.hpp>

using namespace aux;

ShaderCache::ShaderCache()
{
}

ShaderCache::~ShaderCache() throw()
{
	for (auto pair : mCache)
		glDeleteProgram(pair.second);
}

void ShaderCache::enableProgram(const std::string & name)
{
	auto it = mCache.find(name);
	if (it == mCache.end())
		it = createProgram(name);

	if (it == mCache.end())
		return;

	glUseProgram(it->second);
	mLastActiveProgId = it->second;
}

void ShaderCache::applyStandartUniforms(GLuint progId)
{
	const glm::mat4x4 & projMx = SManager()->projectionMx();
	const glm::mat4x4 & viewMx = SManager()->viewMx();

	GLint projUniformId = glGetUniformLocation(progId, "uProjMX");
	GLint viewUniformId = glGetUniformLocation(progId, "uViewMX");

	glUniformMatrix4fv(projUniformId, 1, GL_FALSE, glm::value_ptr(projMx));
	glUniformMatrix4fv(viewUniformId, 1, GL_FALSE, glm::value_ptr(viewMx));
}

std::map<std::string, int>::iterator ShaderCache::createProgram(const std::string & name)
{
	std::string vertName =  name + ".vsh";
	vertName = AbstractDevice::bundlePath(vertName);
	std::string fragName = name + ".fsh";
	fragName = AbstractDevice::bundlePath(fragName);

	std::string vertShaderStr = readFile(vertName);
	std::string fragShaderStr = readFile(fragName);

	GLuint vertShaderId = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShaderId = glCreateShader(GL_FRAGMENT_SHADER);

	const GLchar * vertCharStr = vertShaderStr.c_str();
	glShaderSource(vertShaderId, 1, &vertCharStr, nullptr);
	const GLchar * fragCharStr = fragShaderStr.c_str();
	glShaderSource(fragShaderId, 1, &fragCharStr, nullptr);

	// Компиляция вершинного шейдера
	glCompileShader(vertShaderId);
	GLint vertIsCompiled = 0;
    glGetShaderiv(vertShaderId, GL_COMPILE_STATUS, &vertIsCompiled);
    if (!vertIsCompiled) {
		printf("%s\n", printLogForShader(vertShaderId).c_str());
		glDeleteShader(vertShaderId);
		glDeleteShader(fragShaderId);
        return mCache.end();
    }

	// Компиляция фрагментного шейдера
	glCompileShader(fragShaderId);
	GLint fragIsCompiled = 0;
    glGetShaderiv(fragShaderId, GL_COMPILE_STATUS, &fragIsCompiled);
    if (!fragIsCompiled) {
		printf("%s\n", printLogForShader(fragShaderId).c_str());
		glDeleteShader(vertShaderId);
		glDeleteShader(fragShaderId);
        return mCache.end();
    }

	// Линковка программы
	GLuint programId = glCreateProgram();
	glAttachShader(programId, vertShaderId);
	glAttachShader(programId, fragShaderId);

	glLinkProgram(programId);

	// Применение стандартных униформов к программе
	glUseProgram(programId);
	applyStandartUniforms(programId);

	auto pair = mCache.insert(std::make_pair(name, programId));
	return pair.first;
}

std::string ShaderCache::printLogForShader(GLint shaderId)
{
    int len = 0;

    int written = 0;
    char* info;

    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &len);

    if (len > 0) {
        info = (char*)malloc(len);
        glGetShaderInfoLog(shaderId, len, &written, info);

        std::string str = info;

        free(info);

        return str;
    }

    return "";
}
