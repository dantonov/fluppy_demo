//
//  Fluppy.h
//  fluppy
//
//  Created by Dmitry Antonov on 24/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef Fluppy_h
#define Fluppy_h

#include "GameObject.h"
#include "aux.h"

/// Птичка, прыгающая вверх/вниз
class Fluppy : public GameObject
{
public:
	Fluppy();
	~Fluppy();

	/// Клик по экрану - надо прыгнуть
	void jump();
	/// Обновляем позицию птички в зависимости от скорости
	void update() override;

	/// Получить радиус птички для рассчета столкновений
	inline float radius() const { return mRadius; }

protected:
	/// Создание буферов для рисования птички
	void refreshBuffers() override;

private:
	/// Можно начать считать ускорение (после первого клика)
	bool mClicked = false;
	/// Текущая вертикальная скорость птички
	float mSpeedY = 0.0f;
	/// Радиус птички
	float mRadius = 30.0f;
};

#endif /* Fluppy_h */
