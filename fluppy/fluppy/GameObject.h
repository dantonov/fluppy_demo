//
//  GameObject.h
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef GameObject_h
#define GameObject_h

#include "Callable.h"
#include <stdio.h>
#include "aux.h"
#include <vector>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#include <glm/glm.hpp>

using namespace aux;

class GameObject : public Callable
{
public:
	GameObject();
	virtual ~GameObject() throw();

	/// Задать позицию
	void setPosition(const Position & pos);
	/// Задать позиционирование и размер
	void setRect(const Rect & rect);
	/// Место которое занимает объект в родителе
	Rect rect() const;
	/// Указать что нужно обновить данные для отрисовки
	void setRefreshBuffersDataTrue();
	/// Задать добавочный цвет
	void setAdditiveColor(const Color & color);

	/// Обновление состояния этого и вложенных в него объектов
	virtual void update();
	/// Отрисовка этого и вложенных в него объектов
	void draw();
	/// Вызывается после update() для проверки статуса(пересечений и прочего) этого и вложенных объектов
	virtual void check();

	/// Добавить потомка
	void addChild(GameObject * obj);
	/// Удалить себя из родителя
	void removeFromParent();

protected:
	// Рисование
	/// Нужно ли обновлять VBO
	bool mRefreshBuffersData = true;
	/// Id буфера вершин
	GLuint mVertexBufferId = 0;
	/// Id буфера индексов
	GLuint mIndexBufferId = 0;
	/// Количество индексов записанное в refreshBuffers()
	unsigned long mIndexSize = 0;

	/// Обновить буферы необходимые для рисования
	/// вызывается из draw() при mRefreshDrawData = true
	/// после этого ставит ее в false
	virtual void refreshBuffers();
	/// Выполнить отрисовку этого объекта
	virtual void doDraw();

private:
	/// Родитель
	GameObject * mParent = nullptr;
	/// Положение и размер
	Rect mRect;
	/// Контент этого объекта - другие объекты
	std::vector<std::unique_ptr<GameObject>> mObjects;

	/// Матрица модели
	glm::mat4x4 mModelMx;
	/// Добавочный цвет
	Color mAdditiveColor = {0, 0, 0};
	/// Имя шейдера отрисовки
	std::string mShaderName = "simple";
};

#endif /* GameObject_h */
