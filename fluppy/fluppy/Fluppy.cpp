//
//  Fluppy.c
//  fluppy
//
//  Created by Dmitry Antonov on 24/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#include "Fluppy.h"
#include <cmath>

Fluppy::Fluppy()
{
	setAdditiveColor(colorGreen);
}

Fluppy::~Fluppy()
{
}

void Fluppy::jump()
{
	mClicked = true;
	mSpeedY = 200.0f;
}

void Fluppy::update()
{
	if (!mClicked)
		return;

	// Исходя из обновления 1/30 секунды
	mSpeedY -= 300.0f * 0.034f;
	float delta = mSpeedY * 0.034f;

	Rect rect = GameObject::rect();
	rect.pos.y += delta;
	setRect(rect);
}

void Fluppy::refreshBuffers()
{
	const unsigned numOfVerts = 21;
	const unsigned anglePerVert = 360 / 20;

	// Кружок из вершин
	std::vector<Position> vertices;
	for (unsigned i = 0, angle = 0; i < numOfVerts; ++i)
	{
		double rad = (double)angle / 180.0 * 3.1415;
		float x = mRadius * sin(rad);
		float y = mRadius * cos(rad);
		vertices.push_back(Position(x, y));

		angle += anglePerVert;
	}

	// Индексы для круга вершин
	std::vector<unsigned short> indices;
	for (unsigned i = 0;;)
	{
		unsigned short p1 = i;
		unsigned short p2 = p1 + 1;
		unsigned short p3 = numOfVerts - i;
		unsigned short p4 = p3 - 1;

		std::vector<unsigned short> forInsert = {p1, p2, p3, p2, p3, p4};
		indices.insert(indices.end(), forInsert.begin(), forInsert.end());

		if (i + 1 >= numOfVerts)
			break;

		i += 1;
	}

	mIndexSize = indices.size();

	glGenBuffers(1, &mVertexBufferId);
	glGenBuffers(1, &mIndexBufferId);

	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferId);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Position) * vertices.size(), &vertices[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * mIndexSize, &indices[0], GL_DYNAMIC_DRAW);

	mRefreshBuffersData = false;
}
