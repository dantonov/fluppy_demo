//
//  AbstractDevice.h
//  fluppy
//
//  Created by Dmitry Antonov on 24/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef AbstractDevice_h
#define AbstractDevice_h

#include <stdio.h>
#include <string>
#include "aux.h"

/// Класс - прослойка между функционалом системы и игрой
class AbstractDevice
{
public:
	/// Получить директорию приложения на устройстве
	static std::string bundlePath(const std::string & fileName);
};

#endif /* AbstractDevice_h */
