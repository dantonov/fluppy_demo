//
//  TaskCaller.h
//  fluppy
//
//  Created by Dmitry Antonov on 26/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef TaskCaller_h
#define TaskCaller_h

#include "TaskDelegate.h"
#include "Callable.h"
#include <deque>

/// Служит для отложенных вызовов в потоке
class TaskCaller
{
public:
	inline TaskCaller() {};
	inline virtual ~TaskCaller() throw() {}

	/// Вызвать отложенно метод с аргументами
	template <typename TTYPE, typename ...ARGS>
	inline void run(Callable * target, void(TTYPE::*METHOD)(ARGS...), ARGS... args)
	{
		typedef void (Callable::*CALLTYPE)(ARGS...);
		CALLTYPE method = reinterpret_cast<CALLTYPE>(METHOD);
		TaskDelegate<ARGS...> * delegate = new TaskDelegate<ARGS...>(target, method, args...);
		mAddQueue.push_back(delegate);
	}

	/// Вызывается по таймеру или в конце кадра
	void step();

private:
	/// Очередь вызовов
	std::deque<ADelegate *> mCallQueue;
	/// Очередь на добавление в очередь вызовов
	std::deque<ADelegate *> mAddQueue;
};

#endif /* TaskCaller_h */
