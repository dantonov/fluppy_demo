//
//  aux.h
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef aux_h
#define aux_h

#include <stdio.h>
#include <string>
#include <OpenGLES/ES2/glext.h>
#include <vector>

namespace aux
{

struct Size
{
	float width = 0.0f;
	float height = 0.0f;
	inline Size() {}
	inline Size(float w, float h) : width(w), height(h) {}
	inline Size(const Size & size) : width(size.width), height(size.height) {}
	inline Size operator+(const Size & size) const { return Size(width - size.width, height - size.height); }
	inline Size operator-(const Size & size) const { return Size(width - size.width, height - size.height); }
	inline bool isZero() const { return width == 0.0f && height == 0.0f; }
	inline bool operator==(const Size & size) const { return width == size.width && height == size.height; }
	inline bool operator!=(const Size & size) const { return width != size.width || height != size.height; }
};

struct Position
{
	GLfloat x = 0.0f;
	GLfloat y = 0.0f;
	GLfloat z = 0.0f;

	inline Position() {}
	inline Position(float x_, float y_) : x(x_), y(y_) {}
	inline Position(const Position & pos) : x(pos.x), y(pos.y) {}
	inline Position(const Size & size) : x(size.width), y(size.height) {}
	inline Position operator+(const Position & pos) const { return Position(x + pos.x, y + pos.y);}
	inline Position operator+(const Size & size) const { return Position(x + size.width, y + size.height);}
	inline Position operator+(float value) const { return Position(x + value, y + value);}
	inline Position operator-(const Position & pos) const { return Position(x - pos.x, y - pos.y);}
	inline Position operator-(const Size & size) const { return Position(x - size.width, y - size.height);}
	inline Position operator-(float value) const { return Position(x - value, y - value);}
	inline bool isZero() const { return x == 0.0f  && y == 0.0f; }
	inline bool operator==(const Position & pos) const { return x == pos.x && y == pos.y; }
	inline bool operator!=(const Position & pos) const { return x != pos.x || y != pos.y; }
};

struct Rect
{
	Position pos;
	Size size;

	inline Rect() {}
	inline Rect(float x, float y, float w, float h) : pos(x, y), size(w, h) {}
	inline Rect(const Position & pos_, const Size & size_) : pos(pos_), size(size_) {}
	inline Rect(const Rect & rect) : pos(rect.pos), size(rect.size) {}
	inline Rect operator+(const Rect & rect) const { return Rect(pos + rect.pos, size + rect.size); }
	inline Rect operator-(const Rect & rect) const { return Rect(pos - rect.pos, size - rect.size); }
	inline bool operator==(const Rect & rect) const { return pos == rect.pos && size == rect.size; }
	inline bool operator!=(const Rect & rect) const { return pos != rect.pos || size != rect.size; }
};

struct Color
{
	uint8_t r = 0; // 0-255
	uint8_t g = 0; // 0-255
	uint8_t b = 0; // 0-255

	inline Color() {};
	inline Color(uint8_t r_, uint8_t g_, uint8_t b_) : r(r_), g(g_), b(b_) {}
	inline std::vector<GLfloat> toGlColor() const { return {(GLfloat) r / 255.0f, (GLfloat) g / 255.0f, (GLfloat) b / 255.0f}; }
};

extern Color colorWhite;
extern Color colorGreen;
extern Color colorRed;

/// Пересекается ли отрезок и окружность
bool intersectsWithCircle(const Position & pos1, const Position & pos2, const Position & circPos, double radius);
/// Прочитать содержимое файла в строку
std::string readFile(const std::string & filePath);

} // End of namespace

#endif /* aux_h */
