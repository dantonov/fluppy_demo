//
//  IOSDevice.cpp
//  fluppy
//
//  Created by Dmitry Antonov on 24/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#include "AbstractDevice.h"
#include <cstdlib>
#import <Foundation/Foundation.h>
#import <UIKit/UIDevice.h>

std::string AbstractDevice::bundlePath(const std::string & fileName)
{
	NSString* fullpath =
		[[NSBundle mainBundle] pathForResource:[NSString stringWithUTF8String: fileName.c_str()]
			ofType:nil inDirectory:[NSString stringWithUTF8String: ""]];

	return [fullpath UTF8String];
}
