//
//  AppDelegate.h
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) ViewController *rootVC;


@end

