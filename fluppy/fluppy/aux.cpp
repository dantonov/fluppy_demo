//
//  aux.cpp
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#include "aux.h"
#include <cstdlib>
#include <cmath>

namespace aux
{

Color colorWhite(255, 255, 255);
Color colorGreen(0, 255, 0);
Color colorRed(255, 0, 0);

bool intersectsWithCircle(const Position & pos1, const Position & pos2, const Position & circPos, double radius)
{
	double a = (pos2.x - pos1.x) * (pos2.x - pos1.x) + (pos2.y - pos1.y) * (pos2.y - pos1.y);
	double b = 2 * ((pos2.x - pos1.x) * (pos1.x - circPos.x) + (pos2.y - pos1.y) * (pos1.y - circPos.y));
	double c = circPos.x * circPos.x  + circPos.y * circPos.y + pos1.x * pos1.x + pos1.y * pos1.y - 
		2 * (circPos.x * pos1.x + circPos.y * pos1.y) - radius * radius;
 
	//т.е. если если есть отрицательные корни, то пересечение есть. анализируем теорему виета и формулу корней
	//на предмет отрицательных корней
	if (-b < 0)
		return (c < 0);

	if (-b < (2 * a))
		return (4 * a * c - b * b < 0);

	return (a + b + c < 0);
}

std::string readFile(const std::string & filePath)
{
	FILE * file = fopen(filePath.c_str(), "r");
	if (!file)
		return "";

	fseek(file, 0, SEEK_END);
	size_t size = ftell(file);
	fseek(file, 0, SEEK_SET);

	char *buff = static_cast<char *>(malloc(size * sizeof(unsigned char)));
	fread(buff, sizeof(unsigned char), size, file);
	fclose(file);

	std::string str(buff, size);
	free(buff);

//	printf("%s", str.c_str());
	return str;
}

}// End of namespace
