//
varying highp vec4 vAdditiveColor;

void main()
{
    highp vec4 color = vAdditiveColor;

	gl_FragColor = color;
}
