//
uniform mat4 uModelMX;
uniform mat4 uViewMX;
uniform mat4 uProjMX;

uniform vec4 uAdditiveColor;

attribute vec4 aPosition;

varying highp vec4 vAdditiveColor;

void main()
{
	vAdditiveColor = uAdditiveColor;

	mat4 MVP = uProjMX * uViewMX * uModelMX;
    gl_Position = MVP * aPosition;
}
