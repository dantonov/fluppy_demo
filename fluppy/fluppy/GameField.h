//
//  GameField.h
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef GameField_h
#define GameField_h

#include <stdio.h>
#include <vector>
#include "GameObject.h"
#include "Fluppy.h"
#include "SimpleSignal.h"

/// Игровое поле с основной механикой игры
class GameField : public GameObject
{
public:
	GameField();
	~GameField() throw();

	/// Обновляет позиции колонн
	void update() override;
	/// Проверяет коллизии
	void check() override;
	/// Получить указатель на птичку
	inline Fluppy * fluppy() { return mFluppy; }

	/// Сигнал о том что игра завершена
	SimpleSignal<int> onRestartGame;

private:
	/// Птица которая прыгает вверх/вниз
	Fluppy * mFluppy = nullptr;
	/// Колонны
	std::vector<GameObject *> mColumns;

	/// Обновить состояние колонн
	void updateColumns();
	/// Добавить новую колонны
	void createColumn();

	/// Посчитать пересечение окружности(точка с радиусом) и прямоугольника
	bool haveIntersection(const Position & pos, float radius, const Rect & rect);
	/// Отложенно вызвать рестарт игры
	void onDelayedRestart(const char * str);
};

#endif /* GameField_h */
