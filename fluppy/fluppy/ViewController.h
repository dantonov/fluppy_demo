//
//  ViewController.h
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GLESView;

@interface ViewController : UIViewController

@property (nonatomic, retain) GLESView* glView;

-(BOOL) shouldAutorotate;
-(UIInterfaceOrientation) preferredInterfaceOrientationForPresentation;

@end

