//
//  ViewController.m
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#import "ViewController.h"
#import "GLESView.h"

@implementation ViewController

@synthesize glView;

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

	CGRect rect = self.view.frame;
	glView = [[[GLESView alloc] initWithFrame: rect] autorelease];
	[self.view addSubview: glView];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(BOOL) shouldAutorotate
{
	return NO;
}

-(UIInterfaceOrientation) preferredInterfaceOrientationForPresentation
{
	return UIInterfaceOrientationPortrait;
}

@end
