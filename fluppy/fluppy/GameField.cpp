//
//  GameField.cpp
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#include "GameField.h"
#include "GameManager.h"
#include "TaskCaller.h"

using namespace aux;

/// Максимальное количество колонн (сверху и снизу как одна колонна)
static const unsigned sMaxColumns = 3;

GameField::GameField()
{
	Size size = SManager()->screenSize();
	setRect(Rect(0, 0, size.width, size.height));

	addChild(mFluppy = new Fluppy());
	mFluppy->setPosition(Position(size.width * 0.2f, size.height * 0.5f));
}

GameField::~GameField() throw()
{
}

void GameField::update()
{
	// Обновление состояния птички
	if (mFluppy)
		mFluppy->update();

	updateColumns();

	// Расстояние между колоннами
	const float widthBtwColumns = rect().size.width / sMaxColumns;
	const Size size = rect().size;

	float lastColY = 0.0f;
	if (!mColumns.empty())
	{
		Rect rect = mColumns.back()->rect();
		lastColY = rect.pos.x + rect.size.width;
	}

	// Проверка, можно ли создать очередную колонну
	if (lastColY > size.width - widthBtwColumns)
		return;

	// Создание колонн, если нужно
	createColumn();
}

void GameField::check()
{
	Position fluppyPos = mFluppy->rect().pos;
	float radius = mFluppy->radius();
	for (GameObject * obj : mColumns)
	{
		if (!haveIntersection(fluppyPos, radius, obj->rect()))
			continue;

		SManager()->mainThreadCaller()->run(this, &GameField::onDelayedRestart, __PRETTY_FUNCTION__);
		break;
	}
}

void GameField::updateColumns()
{
	// Обновление и удаление текущих колонн
	const float colSpeed = 40.0f;
	const float colDeltaX = colSpeed * 0.034f;
	for (std::vector<GameObject *>::iterator it = mColumns.begin(); it != mColumns.end();)
	{
		Position objPos = (*it)->rect().pos;
		if (objPos.x + (*it)->rect().size.width < 0.0f)
		{
			(*it)->removeFromParent();
			it = mColumns.erase(it);
			continue;
		}

		objPos.x -= colDeltaX;
		(*it)->setPosition(objPos);

		++it;
	}
}

void GameField::createColumn()
{
	const Size size = rect().size;

	// Далее создание колонны
	// В процентах 30% от общей высоты, 15% - минимальное, макс - 45%
	int topColHeight = rand() % (int)(size.height * 0.3) + (size.height / 100 * 15);
	// Нужно число от 30% от высоты - макс 45%
	int freeSpaceHeight = rand() % (int)(size.height / 100 * 15) + size.height * 0.3;
	// Остаток это нижняя колонна
	int bottomColHeight = size.height - topColHeight - freeSpaceHeight;

	// Верхняя колонна
	GameObject * topColumn = new GameObject();
	addChild(topColumn);
	mColumns.push_back(topColumn);
	topColumn->setAdditiveColor(colorRed);
	// Задаем случайную ширину колонне
	float randWidthTop = rand() % (int)(size.width * 0.1) + size.width * 0.1;
	topColumn->setRect(Rect(size.width, size.height - topColHeight, randWidthTop, topColHeight));

	// Нижняя колонна
	GameObject * bottomColumn = new GameObject();
	addChild(bottomColumn);
	mColumns.push_back(bottomColumn);
	bottomColumn->setAdditiveColor(colorRed);
	float randWidthBottom = rand() % (int)(size.width * 0.2) + size.width * 0.2;
	bottomColumn->setRect(Rect(size.width + 0.5f * (randWidthTop - randWidthBottom), 0.0f,
		randWidthBottom, bottomColHeight));
}

bool GameField::haveIntersection(const Position & circPos, float radius, const Rect & rect)
{
	// Проверка на границы экрана
	// Так как птичка двигается только вертикально, то границу сверяем по Y
	if (circPos.y - radius < 0.0f || circPos.y + radius > GameObject::rect().size.height)
		return true;

	// Далее ищем пересечение отрезков(составляющих колонну) с окружностью
	// Нижняя
	bool inter1 = aux::intersectsWithCircle(rect.pos, rect.pos + Size(rect.size.width, 0), circPos, radius);
	if (inter1)
		return true;
	// Левая
	bool inter2 = aux::intersectsWithCircle(rect.pos, rect.pos + Size(0, rect.size.height), circPos, radius);
	if (inter2)
		return true;
	// верхняя
	bool inter3 = aux::intersectsWithCircle(rect.pos + rect.size, rect.pos + Size(rect.size.width, 0), circPos, radius);
	if (inter3)
		return true;
	// правая
	bool inter4 = aux::intersectsWithCircle(rect.pos + rect.size, rect.pos + Size(0, rect.size.height), circPos, radius);
	if (inter4)
		return true;

	return false;
}

void GameField::onDelayedRestart(const char * str)
{
	printf("CALLED FROM: %s\n", str);
	onRestartGame(1);
}
