//
//  GLESView.m
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#import "GLESView.h"
#include "GameManager.h"
#include "aux.h"

@implementation GLESView

@synthesize renderTimer;

-(id) initWithFrame: (CGRect) rect
{
	if (![super initWithFrame:rect])
		return nil;

	[self setBackgroundColor: [UIColor greenColor]];
	EAGLContext * ctx = [self createContext];
	[EAGLContext setCurrentContext: ctx];

	[self setContext: ctx];
	[self setNeedsDisplay];

	glClearColor(1, 0, 0, 1);

	renderTimer = [NSTimer scheduledTimerWithTimeInterval: 0.034f target:self selector:@selector(setNeedsDisplay) userInfo:nil repeats:YES];

	UITapGestureRecognizer * tapGR = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapCallabck:)] autorelease];
	[self addGestureRecognizer: tapGR];

	return self;
}

-(EAGLContext *) createContext
{
	EAGLContext *context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];
	if (context == nil)
		context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
	return context;
}

-(void) drawRect:(CGRect)rect
{
	if (!SManager())
	{
		GameManager::create();
		SManager()->setScreenSize(aux::Size(rect.size.width, rect.size.height));
	}

	assert(SManager());

	glClear(GL_COLOR_BUFFER_BIT);
	SManager()->update();
}

-(void) dealloc
{
	[renderTimer invalidate];

	if (SManager())
		delete SManager();

	[super dealloc];
}

-(void) onTapCallabck: (UITapGestureRecognizer *) tapGR
{
    if (tapGR.state == UIGestureRecognizerStateRecognized)
    {
        CGPoint point = [tapGR locationInView: tapGR.view];
		if (SManager())
			SManager()->onTap(point.x, point.y);
    }
}

@end
