//
//  ShaderCache.h
//  fluppy
//
//  Created by Dmitry Antonov on 24/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef ShaderCache_h
#define ShaderCache_h

#include <stdio.h>
#include <string>
#include <map>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

/// Кеширует шейдерные программы по названию файла
/// Самостоятельно загружает шейдеры в OpenGL из файлов в бандле
class ShaderCache
{
public:
	ShaderCache();
	~ShaderCache() throw();

	/// Активоровать шейдерную программу (и собрать ее если необходимо)
	void enableProgram(const std::string & name);
	/// Получить ID последней активной программы
	inline GLuint lastActiveProgId() const { return mLastActiveProgId; }
	/// Применить стандартные униформы для текущего шейдера
	void applyStandartUniforms(GLuint progId);

private:
	/// Имя фала - ID программы
	std::map<std::string, GLint> mCache;
	/// Последняя активная программа
	GLuint mLastActiveProgId = 0;

	/// Собрать программу из исходников
	std::map<std::string, GLint>::iterator createProgram(const std::string & name);
	/// Распечатать ошибку компиляции шейдера
	std::string printLogForShader(GLint shaderId);
};

#endif /* ShaderCache_h */
