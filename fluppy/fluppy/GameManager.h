//
//  GameManager.h
//  fluppy
//
//  Created by Dmitry Antonov on 23/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#ifndef GameManager_h
#define GameManager_h

#include <stdio.h>
#include "aux.h"
#include <glm/glm.hpp>
#include "Callable.h"


class GameField;
class WindowSystem;
class ShaderCache;
class TaskCaller;

/// Запускает основные помпоненты игры
/// Осуществляет перезапуск и управление основными ресурсами
class GameManager : public Callable
{
public:
	enum ScheduleAction
	{
		NONE, // Ничего не делать
		START, // Старт игры
		STOP, // Остановка игры
		RESTART // Рестарт игры
	};

	/// Создать менеджер игры
	static void create();
	/// Вернет указатель на текущий менеджер
	static GameManager * instance();

	~GameManager() throw();

	/// Получили клик по экрану
	void onTap(float x, float y);
	/// Запланировать задачу
	void schedule(ScheduleAction action);
	/// Обновить состояние систем
	void update();
	/// Получить размер экрана
	aux::Size screenSize() const;
	/// Задать размер экрана
	void setScreenSize(const aux::Size & size);
	/// Получить матрицу проекции
	const glm::mat4x4 & projectionMx() const;
	/// Получить матрицу вида
	const glm::mat4x4 & viewMx() const;

	/// Получить текущее игровое поле
	GameField * gameField();
	/// Оконная система
	WindowSystem * windowSystem();
	/// Шейдерный кеш
	ShaderCache * shaderCache();
	/// Получить очередь задачь основного потока
	TaskCaller * mainThreadCaller();

private:
	/// Игровое поле, где прыгает птичка
	GameField * mGameField = nullptr;
	/// Оконная система
	WindowSystem * mWindowSystem = nullptr;
	/// Шейдерный кеш
	ShaderCache * mShaderCache = nullptr;
	/// Очередь задачь основного потока
	TaskCaller * mMainCaller = nullptr;

	/// Запланированная задача
	ScheduleAction mCurrentAction = ScheduleAction::NONE;
	/// Размер экрана
	aux::Size mScreenSize;
	/// Матрица проекции
	glm::mat4x4 mProjectionMx;
	/// Матрица вида
	glm::mat4x4 mViewMx;

	/// Отрисовка и обновление контента
	void updateContent();
	/// Выполнить старт игры
	void commitStart();
	/// Выполнить остановку игры
	void commitStop();
	/// Выполнить рестарт игры
	void commitRestart();

	/// Обновить матрицу проекции
	void updateProjectionMx();

	/// Начать новую игру
	void startNewGame(int value);

	GameManager();
};

/// Текущий GameManager
extern GameManager * SManager();

#endif /* GameManager_h */
