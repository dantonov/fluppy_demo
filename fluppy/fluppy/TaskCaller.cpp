//
//  TaskCaller.cpp
//  fluppy
//
//  Created by Dmitry Antonov on 26/01/17.
//  Copyright © 2017 Dmitry Antonov. All rights reserved.
//

#include "TaskCaller.h"

void TaskCaller::step()
{
	// Вызвать методы
	for (ADelegate * del : mCallQueue)
		del->call();

	mCallQueue.clear();

	// Пополнить очередь вызовов
	for (ADelegate  * del : mAddQueue)
		mCallQueue.push_back(del);

	mAddQueue.clear();
}
